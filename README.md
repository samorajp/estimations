## Setup

### Backend

1. Open project directory in IDEA IntelliJ. IDE should Gradle project and download and build stuff.
1. Set up database:
   - from CLI: `docker-compose -f docker-compose-database.yml up`
   - or using IntelliJ `Docker` plugin (to launch `docker-compose-database.yml`)
   - default database URI is `postgresql://postgres:postgres@localhost:5439/estimationsdb`
1. Launch backend
   - via IDEA by running main function in class `EstimationsApplication.kt`
   - or via CLI: `./gradlew bootRun`

## Frontend

1. Open project directory in Visual Studio Code.
1. Inside `ui` directory run command `npm install`.
1. Then, still inside `ui` directory, run command `npm start`.
1. Frontend should be launched and hosted on `http://localhost:4200`.
