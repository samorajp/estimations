import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
	id("org.springframework.boot") version "2.4.0"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	kotlin("jvm") version "1.4.10"
	kotlin("plugin.spring") version "1.4.10"
}

group = "com.syncron"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	jcenter()
	maven(url = "https://dl.bintray.com/kotlin/exposed")
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.liquibase:liquibase-core")
	implementation("org.jetbrains.exposed:exposed-spring-boot-starter:0.20.1")
	implementation("io.springfox:springfox-swagger2:2.9.2")
	implementation("io.springfox:springfox-swagger-ui:2.9.2")

	runtimeOnly("org.postgresql:postgresql")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.register("stage") {
	dependsOn("build")
}

val packageFrontendTask = tasks.create<Copy>("packageFrontend") {
	dependsOn(":ui:buildClient")
	from("${project(":ui").projectDir}/dist")
	into("${buildDir}/classes/kotlin/main/static")
}


tasks["bootJar"].dependsOn(packageFrontendTask.path)

tasks.create<Copy>("unpack") {
	dependsOn("bootJar")
	from(zipTree(tasks["bootJar"].outputs.files.singleFile))
	into("build/dependency")
}

tasks.getByName<BootRun>("bootRun") {
	systemProperties(System.getProperties() as Map<String, Any>)
}


//gradle.taskGraph.whenReady { graph ->
//	if (graph.hasTask(stage)) {
//		test.enabled = false
//	}
//}
