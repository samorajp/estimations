package com.syncron.estimations.estimation.domain.model

data class Task(
    val id: Long = 0,
    val description: String,
    val category: String,
    val storyPoints: Int
)
