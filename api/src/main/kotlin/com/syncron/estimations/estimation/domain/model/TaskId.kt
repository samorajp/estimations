package com.syncron.estimations.estimation.domain.model

data class TaskId(
    val value: Long
)
