package com.syncron.estimations.estimation.domain.ports

import com.syncron.estimations.estimation.domain.model.IndividualEstimation

interface EstimationReadRepository {

    fun findUserEstimation(username: String): IndividualEstimation?

    fun findAll(): List<IndividualEstimation>

}

interface EstimationWriteRepository {

    fun add(estimation: IndividualEstimation): IndividualEstimation

    fun update(estimation: IndividualEstimation): IndividualEstimation

    fun deleteByUsername(username: String)

    fun deleteAll()

}
