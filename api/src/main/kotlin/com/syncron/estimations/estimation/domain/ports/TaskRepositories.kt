package com.syncron.estimations.estimation.domain.ports

import com.syncron.estimations.estimation.domain.model.Task

interface TaskReadRepository {

    fun findBy(id: Long): Task?

    fun findAll(): List<Task>

}

interface TaskWriteRepository {

    fun create(task: Task): Task

    fun save(task: Task)

    fun delete(taskId: Long)

}
