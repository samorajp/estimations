package com.syncron.estimations.estimation.domain.services

import com.syncron.estimations.estimation.domain.ports.EstimationReadRepository
import com.syncron.estimations.estimation.domain.ports.EstimationWriteRepository

class EstimationResetService(
    private val estimationReadRepository: EstimationReadRepository,
    private val estimationWriteRepository: EstimationWriteRepository
) {

    fun clearAllEstimations() {
        estimationWriteRepository.deleteAll()
    }

    fun clearSingleEstimation(username: String) {
        val usernameToDelete = estimationReadRepository.findUserEstimation(username)?.username
        if (usernameToDelete != null) {
            estimationWriteRepository.deleteByUsername(usernameToDelete)
        }
    }

}
