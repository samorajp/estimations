package com.syncron.estimations.estimation.endpoint

import com.syncron.estimations.estimation.domain.model.IndividualEstimation
import com.syncron.estimations.estimation.domain.model.PickSummary
import com.syncron.estimations.estimation.domain.model.TeamSummary

fun TeamSummary.toDto() = TeamSummaryDto(
    average = this.average(),
    minimum = this.minimum(),
    maximum = this.maximum(),
    numberOfTeamMembers = this.numberOfEstimations(),
    commonTasks = this.commonTasks().map { it.toDto() }.toSet(),
    allPickedTasks = this.pickSummaries().map { it.toDto() }.toSet()
)

fun IndividualEstimation.toDto() = IndividualEstimationDto(
    username = this.username,
    storyPointsSum = this.storyPointsSum(),
    pickedTasks = this.pickedTasks.map { it.toDto() }
)

fun PickSummary.toDto() = PickSummaryDto(
    id = this.taskId,
    description = this.description,
    storyPoints = this.storyPoints,
    category = this.category,
    pickedByAll = this.allVoted,
    whoVoted = this.whoVoted.toSet()
)
