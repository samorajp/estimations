package com.syncron.estimations.estimation.endpoint

data class SubmissionDto(
    val username: String,
    val pickedTaskIds: List<Long>
)

data class RemoveEstimationDto(
    val username: String
)

data class IndividualEstimationDto(
    val username: String,
    val storyPointsSum: Int,
    val pickedTasks: List<TaskDto>
)

data class PickSummaryDto(
    val id: Long,
    val description: String,
    val storyPoints: Int,
    val category: String,
    val pickedByAll: Boolean,
    val whoVoted: Set<String>
)

data class TeamSummaryDto(
    val average: Double,
    val minimum: Int,
    val maximum: Int,
    val numberOfTeamMembers: Int,
    val commonTasks: Set<TaskDto> = emptySet(),
    val allPickedTasks: Set<PickSummaryDto> = emptySet()
)
