package com.syncron.estimations.estimation.endpoint

import com.syncron.estimations.API_URI
import com.syncron.estimations.estimation.domain.model.IndividualEstimation
import com.syncron.estimations.estimation.domain.services.EstimationResetService
import com.syncron.estimations.estimation.domain.services.EstimationSubmissionService
import com.syncron.estimations.estimation.endpoint.EstimationEndpoint.Companion.ROOT_PATH
import com.syncron.estimations.estimation.query.EstimationQueryService
import com.syncron.estimations.estimation.query.TaskQueryService
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("$API_URI${ROOT_PATH}")
@Transactional
class EstimationEndpoint(
    private val estimationResetService: EstimationResetService,
    private val estimationSubmissionService: EstimationSubmissionService,
    private val estimationQueryService: EstimationQueryService,
    private val taskQueryService: TaskQueryService
) {

    companion object {
        const val ROOT_PATH = "/estimations"
    }

    @GetMapping
    fun getAllEstimations(): List<IndividualEstimationDto> = estimationQueryService.getAllEstimations().map { it.toDto() }

    @GetMapping("/ready")
    fun teamReady(): Boolean = estimationQueryService.isWholeTeamReady()

    @GetMapping("/summary")
    fun summary(): TeamSummaryDto? = estimationQueryService.getSummary()?.toDto()

    @DeleteMapping
    fun resetEstimations() {
        estimationResetService.clearAllEstimations()
    }

    @DeleteMapping("/{username}")
    fun deleteSingleEstimation(@PathVariable username: String) {
        estimationResetService.clearSingleEstimation(username)
    }

    @PostMapping
    fun submitEstimation(@RequestBody submissionDto: SubmissionDto): IndividualEstimationDto {
        val estimationToSubmit = IndividualEstimation(
            username = submissionDto.username,
            pickedTasks = taskQueryService.getAllByIds(submissionDto.pickedTaskIds)
        )
        return estimationSubmissionService.submitEstimation(estimationToSubmit).toDto()
    }

    @PutMapping
    fun updateEstimation(@RequestBody submissionDto: SubmissionDto): IndividualEstimationDto {
        val estimationToUpdate = IndividualEstimation(
            username = submissionDto.username,
            pickedTasks = taskQueryService.getAllByIds(submissionDto.pickedTaskIds)
        )
        return estimationSubmissionService.updateEstimation(estimationToUpdate).toDto()
    }
}
