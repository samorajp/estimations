package com.syncron.estimations.estimation.endpoint

data class TaskDto(
    val id: Long,
    val description: String,
    val category: String,
    val storyPoints: Int
)

data class CreateTaskDto(
    val description: String,
    val category: String,
    val storyPoints: Int
)
