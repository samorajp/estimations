package com.syncron.estimations.estimation.infrastructure

import com.syncron.estimations.estimation.domain.ports.EstimationReadRepository
import com.syncron.estimations.estimation.domain.services.EstimationResetService
import com.syncron.estimations.estimation.domain.services.EstimationSubmissionService
import com.syncron.estimations.estimation.domain.ports.EstimationWriteRepository
import com.syncron.estimations.estimation.infrastructure.persistance.SqlEstimationReadRepository
import com.syncron.estimations.estimation.infrastructure.persistance.SqlEstimationWriteRepository
import com.syncron.estimations.estimation.query.EstimationQueryService
import com.syncron.estimations.users.query.UserQueryService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.syncron.estimations.estimation"])
class EstimationConfiguration {

    @Bean
    fun estimationResetService(
        estimationReadRepository: EstimationReadRepository,
        estimationWriteRepository: EstimationWriteRepository
    ): EstimationResetService = EstimationResetService(estimationReadRepository, estimationWriteRepository)

    @Bean
    fun estimationSubmissionService(
        estimationWriteRepository: EstimationWriteRepository
    ): EstimationSubmissionService = EstimationSubmissionService(estimationWriteRepository)

    @Bean
    fun estimationQueryService(
        estimationReadRepository: EstimationReadRepository,
        userQueryService: UserQueryService
    ): EstimationQueryService = EstimationQueryService(estimationReadRepository, userQueryService)

    @Bean
    fun estimationReadRepository(): EstimationReadRepository = SqlEstimationReadRepository()

    @Bean
    fun estimationWriteRepository(): EstimationWriteRepository = SqlEstimationWriteRepository()

}
