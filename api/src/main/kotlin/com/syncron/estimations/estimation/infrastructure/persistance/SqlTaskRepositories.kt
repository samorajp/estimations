package com.syncron.estimations.estimation.infrastructure.persistance

import com.syncron.estimations.estimation.domain.model.Task
import com.syncron.estimations.estimation.domain.ports.TaskReadRepository
import com.syncron.estimations.estimation.domain.ports.TaskWriteRepository
import com.syncron.estimations.shared.infrastructure.selectSingleOrNull
import com.syncron.estimations.shared.infrastructure.updateExactlyOne
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll

class SqlTaskReadRepository : TaskReadRepository {

    override fun findBy(id: Long): Task? = TasksTable.selectSingleOrNull { TasksTable.id eq id }?.toTask()

    override fun findAll(): List<Task> = TasksTable.selectAll().map { it.toTask() }
}

class SqlTaskWriteRepository : TaskWriteRepository {

    override fun create(task: Task): Task {
        TasksTable.insert {
            it[id] = task.id
            it[description] = task.description
            it[category] = task.category
            it[storyPoints] = task.storyPoints
        }
        return task
    }

    override fun save(task: Task) {
        TasksTable.updateExactlyOne({ TasksTable.id eq task.id }) {
            it[description] = task.description
            it[category] = task.category
            it[storyPoints] = task.storyPoints
        }
    }

    override fun delete(taskId: Long) {
        TasksTable.deleteWhere { TasksTable.id eq taskId }
    }
}
