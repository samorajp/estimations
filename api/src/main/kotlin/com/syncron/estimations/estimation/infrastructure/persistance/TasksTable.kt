package com.syncron.estimations.estimation.infrastructure.persistance

import org.jetbrains.exposed.sql.Table

object TasksTable : Table("tasks") {
    val id = long("id")
    val description = text("description")
    val category = text("category")
    val storyPoints = integer("story_points")

    override val primaryKey = PrimaryKey(id, name = "TasksTablePKConstraint")
}
