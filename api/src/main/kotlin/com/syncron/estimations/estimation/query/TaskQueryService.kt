package com.syncron.estimations.estimation.query

import com.syncron.estimations.estimation.domain.model.Task
import com.syncron.estimations.estimation.domain.ports.TaskReadRepository

class TaskQueryService(
    private val taskReadRepository: TaskReadRepository
) {

    fun getAll(): List<Task> = taskReadRepository.findAll()

    fun getAllByIds(ids: List<Long>): List<Task> = getAll().filter { ids.contains(it.id) }

}
