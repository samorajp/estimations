package com.syncron.estimations.users.endpoint

import com.syncron.estimations.users.domain.User

data class UserDto(
    val username: String,
    val active: Boolean
)

data class UpdateUserRequest(
    val active: Boolean
)

data class RegisterRequest(
    val username: String
)

data class LoginRequest(
    val username: String
)

fun User.toDto() = UserDto(
    username = this.username,
    active = this.active
)
