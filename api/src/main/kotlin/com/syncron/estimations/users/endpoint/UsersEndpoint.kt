package com.syncron.estimations.users.endpoint

import com.syncron.estimations.API_URI
import com.syncron.estimations.users.domain.UserLoginService
import com.syncron.estimations.users.domain.UserRegisterService
import com.syncron.estimations.users.domain.UserUpdateService
import com.syncron.estimations.users.query.UserQueryService
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("${API_URI}${UsersEndpoint.ROOT_PATH}")
class UsersEndpoint(
    private val userLoginService: UserLoginService,
    private val userRegisterService: UserRegisterService,
    private val userUpdateService: UserUpdateService,
    private val userQueryService: UserQueryService
) {

    companion object {
        const val ROOT_PATH = "/users"
        const val LOGIN_PATH = "/login"
    }

    @PostMapping(LOGIN_PATH)
    fun login(@RequestBody loginRequest: LoginRequest): UserDto =
        userLoginService.authenticate(loginRequest.username).toDto()

    @PostMapping
    fun register(@RequestBody registerRequest: RegisterRequest): UserDto =
        userRegisterService.register(registerRequest.username).toDto()

    @PutMapping
    fun updateUser(@RequestBody userDto: UserDto): UserDto = userUpdateService.update(userDto).toDto()

    @GetMapping
    fun getAll() = userQueryService.findAll().map { it.toDto() }

    @GetMapping("/active")
    fun getActiveUsers() = userQueryService.findAllActiveUsers().map { it.toDto() }

}
