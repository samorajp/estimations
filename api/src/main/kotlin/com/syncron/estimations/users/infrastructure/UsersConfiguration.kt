package com.syncron.estimations.users.infrastructure

import com.syncron.estimations.users.domain.UserLoginService
import com.syncron.estimations.users.domain.UserReadRepository
import com.syncron.estimations.users.domain.UserRegisterService
import com.syncron.estimations.users.domain.UserUpdateService
import com.syncron.estimations.users.domain.UserWriteRepository
import com.syncron.estimations.users.query.UserQueryService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.syncron.estimations.users"])
internal class UsersConfiguration {

    @Bean
    fun userRegisterService(userWriteRepository: UserWriteRepository): UserRegisterService =
        UserRegisterService(userWriteRepository)

    @Bean
    fun userUpdateService(userReadRepository: UserReadRepository, userWriteRepository: UserWriteRepository)
            : UserUpdateService = UserUpdateService(userReadRepository, userWriteRepository)

    @Bean
    fun userLoginService(userReadRepository: UserReadRepository, userWriteRepository: UserWriteRepository)
            : UserLoginService = UserLoginService(userReadRepository, userWriteRepository)

    @Bean
    fun userQueryService(userReadRepository: UserReadRepository): UserQueryService =
        UserQueryService(userReadRepository)

    @Bean
    fun userWriteRepository(): UserWriteRepository = SqlUserWriteRepository()

    @Bean
    fun userReadRepository(): UserReadRepository = SqlUserReadRepository()
}
