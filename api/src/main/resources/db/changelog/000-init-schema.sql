--liquibase formatted sql

--changeset init-schema:1 author:qiubix
create table users(
	username varchar(255) primary key,
	active boolean
);

--changeset init-schema:2 author:qiubix
create table tasks(
	id bigint primary key,
	description varchar(255),
	category varchar(255),
	story_points int
);

create table estimations(
	username varchar(255) primary key
);

create table picked_tasks(
	task_id bigint,
	estimation_id varchar(255)
);
