import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { TasksModule } from './tasks/tasks.module';
import { EstimationsModule } from './estimations/estimations.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { AppReducer } from './core/store/app-store';
import { TasksReducer } from './tasks/store/tasks-reducer';
import { EffectsModule } from '@ngrx/effects';
import { TaskEffects } from './tasks/store/tasks-effects';
import { EstimationsReducer } from './estimations/store/estimations-reducer';
import { EstimationsEffects } from './estimations/store/estimations-effects';
import { UsersEffects } from './users/store/user-effects';
import { UsersReducer } from './users/store/user-reducer';
import { UsersModule } from './users/users.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    TasksModule,
    EstimationsModule,
    UsersModule,
    SharedModule,

    /**
     * Store and effects
     */
    StoreModule.forRoot(AppReducer.reducer),
    StoreModule.forFeature('tasksModule', TasksReducer.reducer),
    StoreModule.forFeature('estimationsModule', EstimationsReducer.reducer),
    StoreModule.forFeature('usersModule', UsersReducer.reducer),
    EffectsModule.forRoot([TaskEffects, EstimationsEffects, UsersEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
