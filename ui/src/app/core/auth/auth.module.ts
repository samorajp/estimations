import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { AlreadyLoggedInGuard } from './already-logged-in.guard';
import { SharedModule } from 'src/app/shared/shared.module';
import { RegisterFormComponent } from './register-form/register-form.component';
import { RegisterDialogComponent } from './register-dialog/register-dialog.component';

@NgModule({
  declarations: [LoginComponent, RegisterFormComponent, RegisterDialogComponent],
  imports: [AuthRoutingModule, SharedModule],
  providers: [AuthGuard, AlreadyLoggedInGuard, AuthService],
})
export class AuthModule {}
