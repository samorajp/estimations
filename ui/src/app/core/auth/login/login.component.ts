import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { RegisterDialogComponent } from '../register-dialog/register-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  form: FormGroup;
  message: string;
  errorMessage: string;
  authorizationInProgress = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
    });
  }

  authorize(): void {
    if (!this.form.valid) {
      this.errorMessage = 'Error!';
      return;
    }

    if (this.authorizationInProgress) {
      return;
    } else {
      this.authorizationInProgress = true;
    }

    this.authService
      .authorize(this.form.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data) => {
          const redirectUrl = this.authService.redirectUrl ? this.authService.redirectUrl : '/';
          this.router.navigate([redirectUrl]);
        },
        (error) => {
          this.authorizationInProgress = false;
          this.message = 'Error!';
        }
      );
  }

  register(): void {
    this.dialog
      .open(RegisterDialogComponent, { disableClose: true })
      .afterClosed()
      .subscribe((result) => {
        console.log('after register: ', result);
        if (result) {
          this.authService
            .registerAndLogin(result)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(
              (data) => {
                const redirectUrl = this.authService.redirectUrl
                  ? this.authService.redirectUrl
                  : '/';
                this.router.navigate([redirectUrl]);
              },
              (error) => {
                this.authorizationInProgress = false;
                this.message = 'Error!';
              }
            );
        }
      });
  }

  get registeredUsers(): Observable<string[]> {
    return this.authService.availableUsers;
  }

  logout(): void {
    this.authService.logout();
  }
}
