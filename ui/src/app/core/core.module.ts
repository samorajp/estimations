import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule } from './auth/auth.module';
import { MaterialModule } from '../shared/material/material.module';
import { MainMenuBarComponent } from './main-menu-bar/main-menu-bar.component';
import { MainComponent } from './main/main.component';
import { RouterModule } from '@angular/router';
import { UserMenuComponent } from './user-menu/user-menu.component';

@NgModule({
  declarations: [MainComponent, MainMenuBarComponent, UserMenuComponent],
  imports: [AuthModule, CommonModule, RouterModule, MaterialModule],
  exports: [AuthModule, MainComponent],
})
export class CoreModule {}
