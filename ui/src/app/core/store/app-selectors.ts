import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Estimations } from 'src/app/estimations/store/estimations-reducer';
import { Tasks } from 'src/app/tasks/store/tasks-reducer';
import { Users } from 'src/app/users/store/user-reducer';

export namespace AppSelectors {
  export const getTasksModuleState = createFeatureSelector<Tasks.State>('tasksModule');
  export const getEstimationsModuleState = createFeatureSelector<Estimations.State>(
    'estimationsModule'
  );
  export const getUsersModuleState = createFeatureSelector<Users.State>('usersModule');

  export const getAllTasksList = createSelector(
    getTasksModuleState,
    (state: Tasks.State) => state.tasksList
  );

  export const getAvailableTasksList = createSelector(
    getEstimationsModuleState,
    (state: Estimations.State) => state.availableTasks
  );

  export const getPickedTasksList = createSelector(
    getEstimationsModuleState,
    (state: Estimations.State) => state.pickedTasks
  );

  export const getTeamEstimationsList = createSelector(
    getEstimationsModuleState,
    (state: Estimations.State) => state.teamEstimations
  );

  export const getTeamSummary = createSelector(
    getEstimationsModuleState,
    (state: Estimations.State) => state.summary
  );

  export const getUserReady = createSelector(
    getEstimationsModuleState,
    (state: Estimations.State) => state.ownEstimationSubmitted
  );

  export const getWholeTeamReady = createSelector(
    getEstimationsModuleState,
    (state: Estimations.State) => state.teamReady
  );

  export const getAllUsers = createSelector(
    getUsersModuleState,
    (state: Users.State) => state.allUsers
  );
}
