import { Estimations } from 'src/app/estimations/store/estimations-reducer';
import { Tasks } from 'src/app/tasks/store/tasks-reducer';
import { Users } from 'src/app/users/store/user-reducer';

export namespace App {
  export interface State {
    tasksModule: Tasks.State;
    estimationsModule: Estimations.State;
    usersModule: Users.State;
  }
}

export namespace AppReducer {
  export function reducer(state: App.State, action): App.State {
    switch (action.type) {
      default:
        return { ...state };
    }
  }
}
