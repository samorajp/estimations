import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css'],
})
export class UserMenuComponent implements OnInit {
  isAuthenticated: boolean;
  currentLogin: string;

  constructor(public authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.isAuthenticated = this.authService.isAuthenticated();
    this.currentLogin = this.authService.currentLogin;
  }

  onLogout(): void {
    this.authService.logout();
    this.router.navigate(['auth', 'login']);
  }
}
