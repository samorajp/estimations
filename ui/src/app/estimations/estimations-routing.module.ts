import { NgModule } from '@angular/core';
import { EstimationsComponent } from './root/estimations.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: EstimationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstimationsRoutingModule {}
