import { Category, Task } from '../tasks/tasks.model';

export interface IndividualEstimation {
  username: string;
  pickedTasks: Task[];
  storyPointsSum: number;
}

export interface Submission {
  username: string;
  pickedTaskIds: number[];
}

export interface TeamSummary {
  average: number;
  minimum: number;
  maximum: number;
  sumOfCommon: number;
  numberOfTeamMembers: number;
  commonTasks: Task[];
  allPickedTasks: PickSummary[];
}

export interface PickSummary {
  id?: number;
  description?: string;
  category?: Category;
  storyPoints?: number;
  pickedByAll?: boolean;
  whoVoted?: string[];
}
