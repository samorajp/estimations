import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { interval, Observable, of } from 'rxjs';
import { filter, mergeMap, startWith, switchMap, take, tap } from 'rxjs/operators';
import { Config } from 'src/config';
import { AuthService } from '../core/auth/auth.service';
import { Tasks } from '../tasks/store/tasks-reducer';
import { Task } from '../tasks/tasks.model';
import { IndividualEstimation, Submission, TeamSummary } from './estimations.model';

@Injectable()
export class EstimationsService {
  baseUri = Config.endpoints.estimationsModule;

  constructor(
    private tasksStore: Store<Tasks.State>,
    private authService: AuthService,
    private http: HttpClient
  ) {}

  getCurrentUser(): Observable<string> {
    return of(this.authService.currentLogin);
  }

  getAvailableTasks(): Observable<Task[]> {
    // console.log('getting available tasks');
    // return this.tasksStore.pipe(select(AppSelectors.getAllTasksList));
    return this.http.get<Task[]>(Config.endpoints.tasksModule);
  }

  // submitEstimation(): Observable<IndividualEstimation> {
  //   const pickedTasks$ = this.tasksStore.pipe(select(AppSelectors.getPickedTasksList));
  //   pickedTasks$.pipe(
  //     map((pickedTasks: Task[]) => {
  //       const sum = pickedTasks.map((it) => it.storyPoints).reduce((a, b) => a + b, 0);
  //       const username = this.authService.currentLogin;
  //       return {
  //         username: username,
  //         pickedTasks: pickedTasks,
  //         storyPointsSum: sum,
  //       };
  //     })
  //   );
  //   const payload = {};
  //   return this.http.post<IndividualEstimation>(this.baseUri, payload);
  // }

  submitIndividualEstimation(pickedTaskIds: number[]): Observable<IndividualEstimation> {
    const payload: Submission = {
      username: this.authService.currentLogin,
      pickedTaskIds: pickedTaskIds,
    };
    // console.log('submitting estimation: ', payload);
    return this.http.post<IndividualEstimation>(this.baseUri, payload);
  }

  updateIndividualEstimation(pickedTaskIds: number[]): Observable<IndividualEstimation> {
    const payload: Submission = {
      username: this.authService.currentLogin,
      pickedTaskIds: pickedTaskIds,
    };
    return this.http.put<IndividualEstimation>(this.baseUri, payload);
  }

  deleteEstimation(): Observable<any> {
    const username = this.authService.currentLogin;
    return this.http.delete(this.baseUri + '/' + username);
  }

  resetAll(): Observable<any> {
    return this.http.delete(this.baseUri);
  }

  getAllEstimations(): Observable<IndividualEstimation[]> {
    return this.http.get<IndividualEstimation[]>(this.baseUri);
  }

  isTeamReady(): Observable<boolean> {
    return interval(3000).pipe(
      startWith(0),
      mergeMap(() => this.http.get<boolean>(this.baseUri + '/ready'))
      // tap((it) => console.log('is team ready: ', it)),
      // filter((ready) => ready)
      // take(1)
    );
  }

  getSummary(): Observable<TeamSummary> {
    return interval(3000).pipe(
      startWith(0),
      mergeMap(() => this.http.get<TeamSummary>(this.baseUri + '/summary'))
    );
  }
}
