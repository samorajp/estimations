import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { Task } from 'src/app/tasks/tasks.model';
import { EstimationsActions } from '../../store/estimations-actions';
import { Estimations } from '../../store/estimations-reducer';

@Component({
  selector: 'app-available-tasks-list',
  templateUrl: './available-tasks-list.component.html',
  styleUrls: ['./available-tasks-list.component.scss'],
})
export class AvailableTasksListComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  availableTasks$: Observable<Task[]>;

  @Output() pickTask: EventEmitter<Task> = new EventEmitter<Task>();

  displayedColumns = ['description', 'storyPoints', 'category'];

  constructor(private store: Store<Estimations.State>) {}

  ngOnInit(): void {
    this.store.dispatch(new EstimationsActions.LoadAvailableTasks());
    this.availableTasks$ = this.store.pipe(select(AppSelectors.getAvailableTasksList));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  pickTaskAction(task: Task): void {
    this.pickTask.emit(task);
  }
}
