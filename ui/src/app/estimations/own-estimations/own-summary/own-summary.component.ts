import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { Task } from 'src/app/tasks/tasks.model';
import { Estimations } from '../../store/estimations-reducer';

@Component({
  selector: 'app-own-summary',
  templateUrl: './own-summary.component.html',
  styleUrls: ['./own-summary.component.css'],
})
export class OwnSummaryComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  pickedTasks$: Observable<Task[]>;
  storyPointsSum: number = 0;

  constructor(private store: Store<Estimations.State>) {}

  ngOnInit(): void {
    this.pickedTasks$ = this.store.pipe(select(AppSelectors.getPickedTasksList));
    this.pickedTasks$.subscribe((tasks: Task[]) => {
      this.storyPointsSum = tasks.map((task) => task.storyPoints).reduce((a, b) => a + b, 0);
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
