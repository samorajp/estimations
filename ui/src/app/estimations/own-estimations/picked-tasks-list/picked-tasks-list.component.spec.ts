import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickedTasksListComponent } from './picked-tasks-list.component';

describe('PickedTasksListComponent', () => {
  let component: PickedTasksListComponent;
  let fixture: ComponentFixture<PickedTasksListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PickedTasksListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickedTasksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
