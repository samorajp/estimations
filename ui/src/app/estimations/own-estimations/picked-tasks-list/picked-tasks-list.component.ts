import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { Task } from 'src/app/tasks/tasks.model';
import { EstimationsActions } from '../../store/estimations-actions';
import { Estimations } from '../../store/estimations-reducer';

@Component({
  selector: 'app-picked-tasks-list',
  templateUrl: './picked-tasks-list.component.html',
  styleUrls: ['./picked-tasks-list.component.scss'],
})
export class PickedTasksListComponent implements OnInit {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Output() removePick: EventEmitter<Task> = new EventEmitter<Task>();

  dataSource: MatTableDataSource<Task> = new MatTableDataSource<Task>();
  displayedColumns = ['description', 'storyPoints', 'category'];

  pickedTasks$: Observable<Task[]>;

  constructor(private store: Store<Estimations.State>) {}

  ngOnInit(): void {
    this.store.dispatch(new EstimationsActions.LoadPickedTasks());
    this.pickedTasks$ = this.store.pipe(select(AppSelectors.getPickedTasksList));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  removePickAction(task: Task) {
    this.removePick.emit(task);
  }
}
