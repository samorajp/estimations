import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { EstimationsService } from '../estimations.service';
import { EstimationsActions } from './estimations-actions';

import LoadAvailableTasksSuccess = EstimationsActions.LoadAvailableTasksSuccess;
import LoadTeamEstimationsSuccess = EstimationsActions.LoadTeamEstimationsSuccess;
import PickSuccess = EstimationsActions.PickSuccess;
import RemovePickSuccess = EstimationsActions.RemovePickSuccess;
import SubmitEstimationSuccess = EstimationsActions.SubmitEstimationSuccess;
import UpdateEstimationSuccess = EstimationsActions.UpdateEstimationSuccess;
import GetCurrentUserSuccess = EstimationsActions.GetCurrentUserSuccess;
import ResetSubmissionSuccess = EstimationsActions.ResetSubmissionSuccess;
import ResetAllSuccess = EstimationsActions.ResetAllSuccess;
import CheckTeamReadySuccess = EstimationsActions.CheckTeamReadySuccess;
import GetSummarySuccess = EstimationsActions.GetSummarySuccess;
import FailedAction = EstimationsActions.Fail;

@Injectable()
export class EstimationsEffects {
  @Effect()
  GetCurrentUser: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.GetCurrentUser),
    switchMap(() =>
      this.service.getCurrentUser().pipe(
        map((data) => new GetCurrentUserSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  LoadAvailableTasks: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.LoadAvailableTasks),
    switchMap(() =>
      this.service.getAvailableTasks().pipe(
        map((data) => new LoadAvailableTasksSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  PickTask: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.Pick),
    map((action: EstimationsActions.Pick) => new PickSuccess(action.payload))
  );

  @Effect()
  RemovePickedTask: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.RemovePick),
    map((action: EstimationsActions.RemovePick) => new RemovePickSuccess(action.payload))
  );

  @Effect()
  SubmitEstimation: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.SubmitEstimation),
    switchMap((action: EstimationsActions.SubmitEstimation) =>
      this.service.submitIndividualEstimation(action.payload).pipe(
        map((data) => new SubmitEstimationSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  ResetSubmission: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.ResetSubmission),
    switchMap(() =>
      this.service.deleteEstimation().pipe(
        map((data) => new ResetSubmissionSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  ResetAll: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.ResetAll),
    switchMap(() =>
      this.service.resetAll().pipe(
        map((data) => new ResetAllSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  UpdateEstimation: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.UpdateEstimation),
    switchMap((action: EstimationsActions.UpdateEstimation) =>
      this.service.updateIndividualEstimation(action.payload).pipe(
        map((data) => new UpdateEstimationSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  LoadTeamEstimations: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.LoadTeamEstimations),
    switchMap(() =>
      this.service.getAllEstimations().pipe(
        map((data) => new LoadTeamEstimationsSuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  CheckTeamReady: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.CheckTeamReady),
    switchMap(() =>
      this.service.isTeamReady().pipe(
        map((data) => {
          // TODO: change state only when data changes
          return new CheckTeamReadySuccess(data);
        }),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  @Effect()
  LoadSummary: Observable<Action> = this.actions.pipe(
    ofType(EstimationsActions.types.GetSummary),
    switchMap(() =>
      this.service.getSummary().pipe(
        map((data) => new GetSummarySuccess(data)),
        catchError((error) => of(new FailedAction(error)))
      )
    )
  );

  constructor(private actions: Actions, private service: EstimationsService) {}
}
