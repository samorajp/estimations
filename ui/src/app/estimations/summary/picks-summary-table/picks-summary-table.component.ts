import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { PickSummary } from '../../estimations.model';

@Component({
  selector: 'app-picks-summary-table',
  templateUrl: './picks-summary-table.component.html',
  styleUrls: ['./picks-summary-table.component.scss'],
})
export class PicksSummaryTableComponent implements OnInit {
  @Input() title: string;
  @Input() tasks: PickSummary[];

  dataSource: MatTableDataSource<PickSummary> = new MatTableDataSource<PickSummary>();
  selection = new SelectionModel<PickSummary>(true, []);
  displayedColumns = [
    'select',
    'description',
    'storyPoints',
    'category',
    'pickedByAll',
    'whoVoted',
  ];

  constructor(private _snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.dataSource.data = this.tasks;
    this.tasks.filter((it) => it.pickedByAll).forEach((it) => this.selection.select(it));
  }

  isAllSelected() {
    return this.selection.selected.length === this.dataSource.data.length;
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.selectAll();
  }

  selectAll() {
    this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  checkboxLabel(row?: PickSummary): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    } else {
      return `${this.selection.isSelected(row) ? 'select' : 'deselect'}`;
    }
  }

  getCopiedValue(): string {
    const title = 'h3. Estimation components\n';
    const header = '||Component||Story points||\n';
    const components = this.selection.selected
      .map((task) => '|' + task.description + '|' + task.storyPoints + ' SP|\n')
      .reduce((prev, curr) => prev + curr, '');
    return title + header + components;
  }

  openSnackBar() {
    // console.log('open snack bar');
    const message = 'Selected components were copied to clipboard.';
    // this.selection.selected
    // .map((it) => it.description + '\n')
    // .reduce((prev, curr) => prev + curr, '');
    this._snackBar.open(message, "Paste it into issue's description", { duration: 4000 });
  }
}
