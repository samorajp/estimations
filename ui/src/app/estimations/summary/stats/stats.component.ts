import { Component, Input, OnInit } from '@angular/core';
import { TeamSummary } from '../../estimations.model';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
})
export class StatsComponent implements OnInit {
  @Input() summary: TeamSummary;

  constructor() {}

  ngOnInit(): void {}
}
