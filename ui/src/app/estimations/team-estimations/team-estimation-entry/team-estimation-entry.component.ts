import { Component, Input, OnInit } from '@angular/core';
import { Task } from 'src/app/tasks/tasks.model';
import { IndividualEstimation } from '../../estimations.model';

@Component({
  selector: 'app-team-estimation-entry',
  templateUrl: './team-estimation-entry.component.html',
  styleUrls: ['./team-estimation-entry.component.css'],
})
export class TeamEstimationEntryComponent implements OnInit {
  tasks: Task[] = [];
  displayedColumns = ['description', 'storyPoints', 'category'];

  @Input() individualEstimation: IndividualEstimation;

  constructor() {}

  ngOnInit(): void {}
}
