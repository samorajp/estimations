import { Component, Input, OnInit } from '@angular/core';
import { IndividualEstimation } from '../../estimations.model';

@Component({
  selector: 'app-team-estimations-list',
  templateUrl: './team-estimations-list.component.html',
  styleUrls: ['./team-estimations-list.component.scss'],
})
export class TeamEstimationsListComponent implements OnInit {
  private _estimations: IndividualEstimation[];

  @Input() teamReady: boolean;

  @Input()
  set estimations(estimations: IndividualEstimation[]) {
    console.log('current estimations: ', estimations);
    this._estimations = estimations;
  }

  get estimations(): IndividualEstimation[] {
    return this._estimations;
  }

  constructor() {}

  ngOnInit(): void {}
}
