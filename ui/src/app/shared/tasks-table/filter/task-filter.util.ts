import { Category, Task } from 'src/app/tasks/tasks.model';

export interface TasksFilter {
  description: string;
  category: string;
}

export class TaskFilterUtil {
  static filterTasks(tasks: Task[], filter: TasksFilter): Task[] {
    return tasks.filter((task: Task) => {
      return (
        this.normalizeString(task.description).includes(this.normalizeString(filter.description)) &&
        this.shouldTaskBeDisplayedByCategory(task.category, filter.category)
      );
    });
  }

  private static shouldTaskBeDisplayedByCategory(current: Category, selected: string): boolean {
    return !selected || (current as Category) === (selected as Category);
  }

  private static normalizeString(value: string): string {
    return value ? value.toLowerCase() : '';
  }
}
