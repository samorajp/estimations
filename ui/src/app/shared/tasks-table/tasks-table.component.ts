import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Task } from 'src/app/tasks/tasks.model';
import { TaskFilterUtil, TasksFilter } from './filter/task-filter.util';

@Component({
  selector: 'app-tasks-table',
  templateUrl: './tasks-table.component.html',
  styleUrls: ['./tasks-table.component.scss'],
})
export class TasksTableComponent implements OnInit {
  @Input()
  set tasks(tasks: Task[]) {
    this.dataSource.data = TaskFilterUtil.filterTasks(tasks, this._currentFilterValue);
    this._tasks = tasks;
  }

  @Input() displayedColumns: string[];
  @Input() hasQuickAdd: boolean;
  @Input() hasFilter: boolean;
  @Input() hasCopyToClipboard: boolean = false;
  @Input() title: string;

  @Output() addNew: EventEmitter<void> = new EventEmitter<void>();
  @Output() delete: EventEmitter<Task> = new EventEmitter<Task>();
  @Output() edit: EventEmitter<Task> = new EventEmitter<Task>();
  @Output() rowClicked: EventEmitter<Task> = new EventEmitter<Task>();

  dataSource: MatTableDataSource<Task> = new MatTableDataSource<Task>();
  private _tasks: Task[] = [];
  private _currentFilterValue: TasksFilter = { description: '', category: null };

  get tasks(): Task[] {
    return this._tasks;
  }

  constructor() {}

  ngOnInit(): void {
    if (this.displayedColumns == null) {
      this.displayedColumns = ['description', 'storyPoints', 'category'];
    }
  }

  onFilterChange(filter: TasksFilter): void {
    this._currentFilterValue = filter;
    this.dataSource.data = TaskFilterUtil.filterTasks(this.tasks, filter);
  }

  addNewTask(): void {
    this.addNew.emit();
  }

  editTask(task: Task): void {
    this.edit.emit(task);
  }

  deleteTask(task: Task): void {
    this.delete.emit(task);
  }

  onRowClicked(task: Task): void {
    this.rowClicked.emit(task);
  }

  getCopiedValue(): string {
    const title = 'h3. Estimation components\n';
    const header = '||Component||Story points||\n';
    const components = this._tasks
      .map((task) => '|' + task.description + '|' + task.storyPoints + ' SP|\n')
      .reduce((prev, curr) => prev + curr);
    return title + header + components;
  }
}
