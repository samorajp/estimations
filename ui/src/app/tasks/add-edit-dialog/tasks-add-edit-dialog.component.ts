import { Component, OnInit, Inject } from '@angular/core';
import { Task } from '../tasks.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-tasks-add-edit-dialog',
  templateUrl: './tasks-add-edit-dialog.component.html',
  styleUrls: ['./tasks-add-edit-dialog.component.scss'],
})
export class TasksAddEditDialogComponent implements OnInit {
  task: Task;

  constructor(
    private dialogRef: MatDialogRef<TasksAddEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private dialogData: Task
  ) {
    this.task = dialogData;
  }

  ngOnInit(): void {}

  onFormOutput(task: Task): void {
    if (task) {
      task.id = this.task.id;
      this.dialogRef.close(task);
    } else {
      this.dialogRef.close();
    }
  }
}
