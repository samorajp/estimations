import { Component, OnInit, Output, Input, EventEmitter, OnDestroy } from '@angular/core';
import { Task } from '../tasks.model';
import { MatDialog } from '@angular/material/dialog';
import { TasksAddEditDialogComponent } from '../add-edit-dialog/tasks-add-edit-dialog.component';
import { Observable, Subject } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Tasks } from '../store/tasks-reducer';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { Papa } from 'ngx-papaparse';
import { saveAs } from 'file-saver';
import { TasksActions } from '../store/tasks-actions';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss'],
})
export class TasksListComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  tasks$: Observable<Task[]>;

  @Output() addNew: EventEmitter<Task> = new EventEmitter<Task>();
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() edit: EventEmitter<Task> = new EventEmitter<Task>();
  @Output() import: EventEmitter<Task[]> = new EventEmitter<Task[]>();

  displayedColumns = ['description', 'storyPoints', 'category', 'options'];

  constructor(
    private dialog: MatDialog,
    private store: Store<Tasks.State>,
    private csvParser: Papa
  ) {}

  ngOnInit(): void {
    // console.log('Init task list component');
    this.store.dispatch(new TasksActions.LoadList());
    this.tasks$ = this.store.pipe(select(AppSelectors.getAllTasksList));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  openAddDialog(): void {
    this.dialog
      .open(TasksAddEditDialogComponent, {
        data: {},
        disableClose: true,
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.addNew.emit(result);
        }
      });
  }

  openEditDialog(task: Task): void {
    this.dialog
      .open(TasksAddEditDialogComponent, {
        data: task,
        disableClose: true,
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.edit.emit(result);
        }
      });
  }

  deleteTask(task: Task): void {
    this.delete.emit(task.id);
  }

  importedTasks = [];

  handleFileSelect(fileSelectEvent: any): void {
    var files: FileList = fileSelectEvent.target.files;
    var file = files[0];
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = (event: any) => {
      var csv = event.target.result;
      this.csvParser.parse(csv, {
        header: true,
        skipEmptyLines: true,
        complete: (results) => {
          for (let i = 0; i < results.data.length; i++) {
            let task: Task = {
              description: results.data[i].description,
              storyPoints: results.data[i].storyPoints,
              category: results.data[i].category,
            };
            this.importedTasks.push(task);
          }
        },
      });
    };
  }

  importTasks(): void {
    console.log('Imported tasks: ', this.importedTasks);
    this.import.emit(this.importedTasks);
  }

  exportTasks(): void {
    this.tasks$.subscribe((tasks) => {
      const replacer = (key, value) => (value === null ? '' : value);
      const header = Object.keys(tasks[0]);
      let csv = tasks.map((task) =>
        header.map((fieldName) => JSON.stringify(task[fieldName], replacer)).join(',')
      );
      csv.unshift(header.join(','));
      let csvArray = csv.join('\r\n');

      var blob = new Blob([csvArray], { type: 'text/csv' });
      saveAs(blob, 'tasks.csv');
    });
  }
}
