import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TasksActions } from '../store/tasks-actions';
import { Tasks } from '../store/tasks-reducer';
import { Task } from '../tasks.model';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit {
  constructor(private store: Store<Tasks.State>) {}

  ngOnInit(): void {}

  onAdd(task: Task): void {
    this.store.dispatch(new TasksActions.Create(task));
  }

  onDelete(id: number): void {
    this.store.dispatch(new TasksActions.Delete(id));
  }

  onEdit(task: Task): void {
    this.store.dispatch(new TasksActions.Update(task));
  }

  onImport(tasks: Task[]): void {
    this.store.dispatch(new TasksActions.CreateMany(tasks));
  }
}
