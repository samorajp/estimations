import { Action } from '@ngrx/store';
import { Task } from '../tasks.model';

export namespace TasksActions {
  export const types = {
    Create: '[Tasks] Create',
    CreateSuccess: '[Tasks] Create successful',
    LoadList: '[Tasks] Load list',
    LoadListSuccess: '[Tasks] Load list success',
    Delete: '[Tasks] Delete',
    DeleteSuccess: '[Tasks] Delete successful',
    Update: '[Tasks] Update',
    UpdateSuccess: '[Tasks] Update successful',
    CreateMany: '[Tasks] Create many',
    CreateManySuccess: '[Tasks] Create many successful',
    Fail: '[Tasks] Action failed',
  };

  export class Create implements Action {
    type = types.Create;
    constructor(public payload: Task) {}
  }

  export class CreateSuccess implements Action {
    type = types.CreateSuccess;
    constructor(public payload: Task) {}
  }

  export class LoadList implements Action {
    type = types.LoadList;
    constructor(public payload?: any) {}
  }

  export class LoadListSuccess implements Action {
    type = types.LoadListSuccess;
    constructor(public payload: Task[]) {}
  }

  export class Delete implements Action {
    type = types.Delete;
    constructor(public payload: number) {}
  }

  export class DeleteSuccess implements Action {
    type = types.DeleteSuccess;
    constructor(public payload: any) {}
  }

  export class Update implements Action {
    type = types.Update;
    constructor(public payload: Task) {}
  }

  export class UpdateSuccess implements Action {
    type = types.UpdateSuccess;
    constructor(public payload: Task) {}
  }

  export class CreateMany implements Action {
    type = types.CreateMany;
    constructor(public payload: Task[]) {}
  }

  export class CreateManySuccess implements Action {
    type = types.CreateManySuccess;
    constructor(public payload: Task[]) {}
  }

  export class Fail implements Action {
    type = types.Fail;
    constructor(public payload: any) {}
  }
}
