export interface Task {
  id?: number;
  description?: string;
  category?: Category;
  storyPoints?: number;
}

export enum Category {
  FRONTEND = 'Frontend',
  BACKEND = 'Backend',
  DATABASE = 'Database',
  MAINTENANCE = 'Maintenance',
  DESIGN = 'Design',
  TESTS = 'Tests',
  REFACTOR = 'Refactor',
  DOCS = 'Docs',
  OTHER = 'Other',
}
