import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Config } from 'src/config';
import { Task } from './tasks.model';

@Injectable()
export class TasksService {
  nextId = 3;
  baseUri = Config.endpoints.tasksModule;

  constructor(private http: HttpClient) {}

  addNew(task: Task): Observable<Task> {
    // const newTask = Object.assign({}, task, { id: this.nextId++ });
    // console.log('adding new with data: ', task);
    // return of(newTask);
    return this.http.post(this.baseUri, task);
  }

  addMany(tasks: Task[]): Observable<Task[]> {
    // console.log('adding many: ', tasks);
    // const newTasks = tasks.map((task) => Object.assign({}, task, { id: this.nextId++ }));
    // console.log('ids assigned: ', newTasks);
    // return of(newTasks);
    return this.http.post<Task[]>(this.baseUri + '/import', tasks);
  }

  getAll(): Observable<Task[]> {
    return this.http.get<Task[]>(this.baseUri);
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(this.baseUri + '/' + id);
  }

  update(task: Task): Observable<Task> {
    return this.http.put<Task>(this.baseUri, task);
  }
}
