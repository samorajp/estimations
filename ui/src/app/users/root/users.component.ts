import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppSelectors } from 'src/app/core/store/app-selectors';
import { UserActions } from '../store/user-actions';
import { Users } from '../store/user-reducer';
import { User } from '../users.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  constructor(private store: Store<Users.State>) {}

  users$: Observable<User[]>;

  ngOnInit(): void {
    this.store.dispatch(new UserActions.LoadList());
    this.users$ = this.store.pipe(select(AppSelectors.getAllUsers));
  }

  onToggleActive(user: User) {
    this.store.dispatch(new UserActions.ToggleActive(user));
  }
}
