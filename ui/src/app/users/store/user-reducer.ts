import { User } from '../users.model';
import { UserActions } from './user-actions';

export namespace Users {
  export interface State {
    currentUser: User;
    allUsers: User[];
  }

  export const initialState: Users.State = {
    currentUser: null,
    allUsers: [],
  };
}

export namespace UsersReducer {
  export function reducer(state: Users.State = Users.initialState, action): Users.State {
    switch (action.type) {
      case UserActions.types.LoadListSuccess: {
        return {
          ...state,
          allUsers: action.payload,
        };
      }

      case UserActions.types.LoginSuccess: {
        return {
          ...state,
          currentUser: action.payload,
        };
      }

      case UserActions.types.ToggleActiveSuccess: {
        return {
          ...state,
          allUsers: state.allUsers.map((user: User) =>
            user.username === action.payload.username ? action.payload : user
          ),
        };
      }

      default:
        return state;
    }
  }
}
