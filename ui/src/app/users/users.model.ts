export interface User {
  username: string;
  active: boolean;
}
