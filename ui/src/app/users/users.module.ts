import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './root/users.component';
import { UserListComponent } from './user-list/user-list.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersService } from './users.service';

@NgModule({
  declarations: [UsersComponent, UserListComponent],
  imports: [SharedModule, UsersRoutingModule],
  exports: [UsersComponent],
  providers: [UsersService],
})
export class UsersModule {}
