import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Config } from 'src/config';
import { User } from './users.model';

@Injectable()
export class UsersService {
  baseUri = Config.endpoints.usersModule;

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUri);
  }

  login(username: string): Observable<User> {
    return this.http.post<User>(this.baseUri + '/login', { username: username });
  }

  register(username: string): Observable<User> {
    return this.http.post<User>(this.baseUri, { username: username });
  }

  toggleActive(user: User): Observable<User> {
    const payload: User = {
      username: user.username,
      active: !user.active,
    };
    return this.http.put<User>(this.baseUri, payload);
  }

  delete(username: string): Observable<string> {
    return of();
  }

  constructor(private http: HttpClient) {}
}
